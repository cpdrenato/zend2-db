<?php

 // Database connection details
    return array(
    'db' => [
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=zend-db;host=localhost',
        'driver_options' => [
            \PDO::MYSQL_ATTR_INIT_COMMAND =>'SET NAMES \'UTF8\''
        ]
    ],

    'service_manager'=> [
        'factories'=> [
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory'
        ]
    ]
);