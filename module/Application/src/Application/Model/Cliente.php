<?php


namespace Application\Model;

class Cliente{
	private $id;
	private $nome;
	private $cep;
	private $endereco;
	private $numero;
	private $complemento;
	private $cidade;
	private $bairro;
	private $estado;
	private $telefone;
	private $email;

	public function getId(){
		return $this->id;
	}
	
	private function setId($id){
		$this->id = $id;
		return $this;

	}
	public function getNome(){
		return $this->nome;

	}
	private function setNome($nome){
		$this->nome = $nome;
		return $this;

	}
	public function getCep(){
		return $this->cep;

	}
	private function setCep($cep){
		$this->cep = $cep;
		return $this;

	}
	public function getEndereco(){
		return $this->endereco;

	}
	private function setEndereco($endereco){
		$this->endereco = $endereco;
		return $this;

	}
	public function getNumero(){
		return $this->numero;

	}
	private function setNumero($numero){
		$this->numero = $numero;
		return $this;

	}
	public function getComplemento(){
		return $this->complemento;

	}
	private function setComplemento($complemento){
		$this->complemento = $complemento;
		return $this;

	}
	public function getCidade(){
		return $this->cidade;

	}
	private function setCidade($cidade){
		$this->cidade = $cidade;
		return $this;

	}
	public function getBairro(){
		return $this->bairro;

	}
	private function setBairro($bairro){
		$this->bairro = $bairro;
		return $this;

	}
	public function getEstado(){
		return $this->estado;

	}
	private function setEstado($estado){
		$this->estado = $estado;
		return $this;

	}
	public function getTelefone(){
		return $this->telefone;

	}
	private function setTelefone($telefone){
		$this->telefone = $telefone;
		return $this;

	}
	public function getEmail(){
		return $this->email;

	}
	private function setEmail($email){
		$this->email = $email;
		return $this;

	}

	public function exchangeArray(array $data){
		$this->setId(isset($data['id'])?$data['id']:0)
		->setNome($data['nome'])
		->setCep($data['cep'])
		->setEndereco($data['endereco'])
		->setNumero($data['numero'])
		->setComplemento($data['complemento'])
		->setCidade($data['cidade'])
		->setBairro($data['bairro'])
		->setEstado($data['estado'])
		->setTelefone($data['telefone'])
		->setEmail($data['email']);

	}
//pega o metado e converte para um array
	public function getArrayCopy(){
		return [
			'id' => $this->getId(),
			'nome' => $this->getNome(),
			'cep' => $this->getCep(),
			'endereco' => $this->getEndereco(),
			'numero' => $this->getNumero(),
			'complemento' => $this->getComplemento(),
			'cidade' => $this->getCidade(),
			'bairro' => $this->getBairro(),
			'estado' => $this->getEstado(),
			'telefone' => $this->getTelefone(),
			'email' => $this->getEmail(),


		];
	}
}